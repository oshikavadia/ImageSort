﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Configuration;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace ImageSort
{
    class Program
    {

        static void Main(string[] args)
        {
            string path;
            if (args.Length == 1)
            {
                path = args[0];

            }
            else
            {
                Console.WriteLine("Check path ");
                return;
            }
            int[,] arr;
            Console.WriteLine("reading image");
            getImage(path, out arr);
            Console.WriteLine("sorting image");
            sortImageArray(ref arr);
            Console.WriteLine("writing image");
            writeImage(path, ref arr);
            Console.WriteLine("done");
        }

        public static void getImage(string path, out int[,] arr)
        {
            try
            {
                Bitmap img = new Bitmap(path);
                arr = new int[img.Width, img.Height];
                for (int x = 0; x < img.Width; x++)
                {
                    for (int y = 0; y < img.Height; y++)
                    {
                        Color pixel = img.GetPixel(x, y);

                        arr[x, y] = pixel.ToArgb();

                    }
                }
            }
            catch (Exception E)
            {
                Console.WriteLine(E);
                arr = null;
            }
        }

        public static void sortImageArray(ref int[,] arr)
        {
            int[][] tempContainer = new int[arr.GetLength(0)][];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                int[] temp = new int[arr.GetLength(1)];
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    temp[j] = arr[i, j];
                }
                Array.Sort(temp);
                tempContainer[i] = temp;
            }

            for (int i = 0; i < tempContainer.Length; i++)
            {
                for (int j = 0; j < tempContainer[i].Length; j++)
                {
                    arr[i, j] = tempContainer[i][j];
                }
            }

        }

        public static void writeImage(string path, ref int[,] arr)
        {
            string pathDirectory = path.Substring(0, path.LastIndexOf("."));
            string pathFileName = Path.GetFileNameWithoutExtension(path);
            Bitmap bmp = new Bitmap(arr.GetLength(0), arr.GetLength(1));

            for (int x = 0; x < arr.GetLength(0); x++)
            {
                for (int y = 0; y < arr.GetLength(1); y++)
                {
                    Color pixel = Color.FromArgb(arr[x, y]);
                    bmp.SetPixel(x, y, pixel);
                }
            }
            try
            {
                Directory.CreateDirectory(pathDirectory + @" - sorted\");
                bmp.Save(pathDirectory + @" - sorted\" + pathFileName + ".jpeg", ImageFormat.Jpeg);
                bmp.Save(pathDirectory + @" - sorted\" + pathFileName + ".png", ImageFormat.Png);
                bmp.Save(pathDirectory + @" - sorted\" + pathFileName + ".tiff", ImageFormat.Tiff);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


        }
    }
}
